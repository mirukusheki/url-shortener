<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use Exception;

class AuthController extends Controller {
  public function showLoginForm() {
    $menu = 'login';
    $data = [
      'menu' => $menu
    ];
    return view('auth.login')->with(['data' => $data]);
  }

  public function loginPost(Request $request) {
    $validation = Validator::make($request->all(), [
      'username' => 'required|alpha_num|min:5',
      'password' => 'required'
    ]);
    if ($validation->fails()) {
      return redirect(route('login'))->withErrors($validation->errors())->withInput($request->all());
    }

    $loginData = [
      'username' => $request->input('username'),
      'password' => $request->input('password'),
    ];
    if (Auth::attempt($loginData, $request->input('remember_me'))) {
      return redirect('/');
    }
    return redirect(route('login'))->withErrors(['general' => 'Login Failed!']);
  }

  public function showRegisterForm() {
    $menu = 'register';
    $data = [
      'menu' => $menu
    ];
    return view('auth.register')->with(['data' => $data]);
  }

  public function registerPost(Request $request) {
    $validation = Validator::make($request->all(), [
      'name'     => 'required',
      'username' => 'required|alpha_num|min:5|unique:users',
      'email'    => 'required|email|unique:users',
      'password' => 'required'
    ]);
    if ($validation->fails()) {
      return redirect(route('register'))->withErrors($validation->errors())->withInput($request->all());
    }
    try {
      User::create([
        'name'     => $request->input('name'),
        'username' => $request->input('username'),
        'email'    => $request->input('email'),
        'password' => Hash::make($request->input('password')),
      ]);
      return redirect(route('register'))->with(['success' => 'User Registered Successfully.']);
    } catch (Exception $exception) {
      return redirect(route('register'))->with(['generalError' => $exception->getMessage()])->withInput($request->all());
    }
  }

  public function logout() {
    Auth::logout();
    return redirect(route('login'))->with(['success' => 'Logged Out!']);
  }
}
