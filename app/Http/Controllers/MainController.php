<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Http\Request;
use Auth;

class MainController extends Controller {
  public function index() {
    $menu = 'home';
    if (Auth::check()){
      $links = Links::orderBy('id', 'DESC')->where('created_by_id',auth()->user()->id)->get();
    }
    else{
      $links = Links::orderBy('id', 'DESC')->where('created_by_id',null)->paginate(5);
    }
    $data = [
      'menu' => $menu,
      'links' => $links
    ];
    return view('home', ['data' => $data])->render();
  }
}
