<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Str;
use Auth;

class LinkController extends Controller {
  public function make(Request $request) {
    $validator = Validator::make($request->all(), array(
      'url' => 'required|url|max:255'
    ));

    if ($validator->fails()) {
      return redirect('/')->withErrors($validator);
    }
    $url  = $request->all()['url'];
    $code = $request->all()['code'];
    if (Auth::check()) {
      $created = Links::create([
        'url'           => $url,
        'created_by_id' => auth()->user()->id
      ]);
    } else {
      $created = Links::create([
        'url' => $url
      ]);
    }
    if ($created) {
      if ($code == null){
        $code = Str::random(6);
      }
      Links::where('id', '=', $created->id)->update(array(
        'code' => $code
      ));
    }
    return redirect('/')->with('global', $code);
  }

  public function open($code) {
    $link = Links::where('code', $code)->first();
    try {
      if (Links::where('code', $code)->count() === 1) {
        $url   = $link->url;
        $visit = $link->times_visit;
        $visit++;
        Links::where('code', $code)->update([
          'times_visit' => $visit
        ]);
        $data = [
          'url' => $url
        ];
        return view('redirect')->with('data', $data)->render();
      }
      throw new Exception;
    } catch (Exception $exception) {
      return view('error');
    }
  }

  public function delete(Request $request, $skey = '') {
    DB::begintransaction();
    try {
      $link = Links::where('skey', $skey)->firstOrFail();
      $link->delete();
      DB::commit();
      $links = Links::orderBy('id', 'DESC')->where('created_by_id',auth()->user()->id)->get();
      $data  = [
        'links' => $links
      ];
      return view('table')->with('data', $data)->render();
    } catch (Exception $exception) {
      DB::rollBack();
      dd($exception);
    }
  }
}
