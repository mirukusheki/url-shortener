<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateLinksTable extends Migration {
  public function up() {
    Schema::create('links', function (Blueprint $table) {
      $table->increments('id');
      $table->uuid('skey')->unique();
      $table->string('url');
      $table->string('code', 12)->unique()->nullable();
      $table->integer('times_visit')->default(0);
      $table->unsignedInteger('created_by_id')->nullable();
      $table->timestamps();
      $table->foreign('created_by_id')->references('id')->on('users')->onDelete('cascade');
    });

    $trigger = 'CREATE TRIGGER create_skey_links BEFORE INSERT
    ON links
    FOR EACH ROW BEGIN
      SET new.skey=UUID();
    END';

    DB::unprepared($trigger);
  }

  public function down() {
    Schema::dropIfExists('links');
  }
}
