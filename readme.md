## Short.it - A Simple Shortener Page
Short.it is a simple shortener site made by Miruku Sheki. This site was created for personal project. for now we cannot publish it to the server but someday will do.

Short.it has 2 types of users, Guest and Users. Guest have fewer features than users

<table border="1">
  <tr>
    <td>Features</td>
    <td>Guest Users</td>
    <td>Account Users</td>
  </tr>
  <tr>
    <td>Create Shortener Link</td>
    <td>Yes</td>
    <td>Yes</td>
  </tr>
  <tr>
    <td>View Times Clicked</td>
    <td>Yes</td>
    <td>Yes</td>
  </tr>
  <tr>
    <td>Personalised Shortener links</td>
    <td>No</td>
    <td>Yes</td>
  </tr>
  <tr>
    <td>Costumised Shortener links</td>
    <td>No</td>
    <td>Yes</td>
  </tr>
  <tr>
    <td>Recent Shortened Table</td>
    <td>Only last 5</td>
    <td>Unlimited</td>
  </tr>
  

</table>