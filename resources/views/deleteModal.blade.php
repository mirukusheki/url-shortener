<div class="modal fade" id="delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Link</h4>
      </div>
      <div class="modal-body">
        Are you Sure You want to delete this URL? This couldn't be undone! This URL will not be accessed again.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok" href="#" id="delete">Delete</a>
      </div>
    </div>
  </div>
</div>