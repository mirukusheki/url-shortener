@php
  $links = $data['links'];
@endphp
<table id="links" class="table">
  <thead>
  <tr>
    <td>No</td>
    <td>Code</td>
    <td>URL</td>
    <td>Times Clicked</td>
    <td>Date Created</td>
    <td></td>
  </tr>
  </thead>
  <tbody>
  @foreach($links as $index=>$l)
    <tr>
      <td>{{$index+1}}</td>
      <td><a href="{{url('/')}}/{{$l->code}}" target="_blank">{{url('/')}}/{{$l->code}}</a></td>
      <td>{{$l->url}}</td>
      <td>{{$l->times_visit}}</td>
      <td>{{$l->created_at}}</td>
      @if(Auth::check())
        <td><a class="delete" skey="{{$l->skey}}" style="cursor: pointer;">Delete</a></td>
      @endif
    </tr>
  @endforeach
  </tbody>
</table>