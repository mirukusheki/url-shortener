<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Shortener Link</title>
  @include('layout.style')
  @include('layout.script')
</head>
<body>
<div style="margin-top:4em;">
  @include('layout.navbar')
  <div class="container">
    @yield('content')
  </div>
  @include('layout.footer')
  @yield('js')
</div>
</body>
</html>