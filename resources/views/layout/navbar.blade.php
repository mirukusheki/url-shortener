<?php
if (isset($data['menu'])) {
  $name = $data['menu'];
} else {
  $name = 'home';
}
?>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li class="navbar-brand">Shortener Link</li>
      <li class="{{$name=='home'?'active':''}}"><a href="/">Home</a></li>
    </ul>
    <ul class="nav navbar-nav pull-right">
      @if(!auth()->check())
        <li class="{{$name=='login'?'active':''}}"><a href="login">Login</a></li>
        <li class="{{$name=='register'?'active':''}}"><a href="{{route('register')}}">Register</a></li>
      @else
        <li><a href="#">Welcome, {{auth()->user()->name}}</a></li>
        <li><a href="{{route('logout')}}">Logout</a></li>
      @endif
    </ul>
  </div>
</nav>