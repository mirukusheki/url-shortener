@extends('layout.template')
@section('content')
  <div class="login-box">
    <h1>Register</h1>
    <form action="{{route('register.post')}}" method="post" class="form-horizontal">
      @csrf
      <div class="form-group row">
        <label class="control-label col-sm-4" for="name">Name: </label>
        <input type="text" class="form-control col-sm-8" id="name" name="name" placeholder="Enter Your Name"
               value="{{old('name')}}">
        @if($errors->has('name'))
          <div class="text-danger">
            <strong>{{$errors->first('name')}}</strong>
          </div>
        @endif
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4" for="username">Username: </label>
        <input type="text" class="form-control col-sm-8" id="username" name="username" placeholder="Enter Username"
               value="{{old('username')}}">
        @if($errors->has('username'))
          <div class="text-danger">
            <strong>{{$errors->first('username')}}</strong>
          </div>
        @endif
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4" for="email">Email: </label>
        <input type="email" class="form-control col-sm-8" id="email" name="email" placeholder="Enter Email"
               value="{{old('email')}}">
        @if($errors->has('email'))
          <div class="text-danger">
            <strong>{{$errors->first('email')}}</strong>
          </div>
        @endif
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4" for="password">Password: </label>
        <input type="password" class="form-control col-sm-8" id="password" name="password" placeholder="Enter Password">
        @if($errors->has('password'))
          <div class="text-danger">
            <strong>{{$errors->first('password')}}</strong>
          </div>
        @endif
        @if($errors->has('general'))
          <div class="text-danger">
            <strong>{{$errors->first('general')}}</strong>
          </div>
        @endif
      </div>
      <div class="form-group row">
        <div class="text-right col-sm-7">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
      @if(session()->has('success'))
        <div class="text-success">
          <strong>User Registered Successfully. Click <a href="{{route('login')}}">here</a> to login!</strong>
        </div>
      @endif
      @if(session()->has('generalError'))
        <div class="text-danger">
          <strong>{{session('generalError')}}</strong>
        </div>
      @endif
    </form>
  </div>
@endsection