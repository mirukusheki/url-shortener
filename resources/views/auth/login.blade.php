@extends('layout.template')
@section('content')
  <div class="login-box">
    <h1>Login</h1>
    <form action="{{route('login.post')}}" method="post" class="form-horizontal">
      @csrf
      <div class="form-group row">
        <label class="control-label col-sm-4" for="username">Username: </label>
        <input type="text" class="form-control col-sm-8" id="username" name="username" placeholder="Enter Username"
               value="{{old('username')}}">
        @if($errors->has('username'))
          <div class="text-danger">
            <strong>{{$errors->first('username')}}</strong>
          </div>
        @endif
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4" for="password">Password: </label>
        <input type="password" class="form-control col-sm-8" id="password" name="password" placeholder="Enter Password">
        @if($errors->has('password'))
          <div class="text-danger">
            <strong>{{$errors->first('password')}}</strong>
          </div>
        @endif
      </div>
      @if($errors->has('general'))
        <div class="text-danger">
          <strong>{{$errors->first('general')}}</strong>
        </div>
      @endif
      @if(session()->has('success'))
        <div class="text-danger">
          <strong>{{session('success')}}</strong>
        </div>
      @endif
      <div class="form-group row">
        <div class="text-right col-sm-5">
          <div class="checkbox">
            <label><input type="checkbox" name="remember_me"> Remember me</label>
          </div>
        </div>
        <div class="text-right col-sm-7">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
  </div>
@endsection