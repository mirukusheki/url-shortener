<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>404 Not Found!</title>
  @include('layout.style')
</head>
<body onload="setTimeout('history.go(-1)', 3000);">
<div class="not-found">
  <h1>404</h1>
  <h2>Sorry, It's not found here</h2>
  <p>Make sure your link is correct.</p>
  <p>Otherwise, we will redirect you back in a few seconds. Or you can click it
    <a onclick="goBack()" style="cursor: pointer;">here.</a></p>
</div>
<small>This URL is created by <a href="/">URL Shortener</a>. 2018. All Rights Reserved.</small>
</body>
</html>
<script>
  function goBack() {
    window.history.back();
  }
</script>