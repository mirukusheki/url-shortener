<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Redirecting...</title>
  @include('layout.style')
</head>
@php
  $url = $data['url'];
@endphp
<body>
<div class="not-found">
  <img src="{{asset('img')}}/redirecting.svg" alt="Redirecting...">
  <h2>Please wait, we will redirecting you</h2>
  <p>We will redirect you back in a few seconds. Or you can just click it
    <a href="{{$url}}" style="cursor: pointer;" target="_blank">here.</a></p>
</div>
<small>This URL is created by <a href="/">URL Shortener</a>. 2018. All Rights Reserved.</small>
<script type="text/javascript"><!--
  setTimeout('Redirect()',3000);
  function Redirect()
  {
    location.href = '{{$url}}';
  }
  // --></script>
</body>
</html>