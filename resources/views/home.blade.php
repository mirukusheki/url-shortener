@extends('layout.template')
@section('content')
  <h1>URL Shortener</h1>
  <h3>You can Shorten your URL Here!</h3>

  <form class="form-inline" action="{{url('make')}}" method="post">
    @csrf
    <input type="text" name="url" placeholder="Enter Your URL" class="form-control" style="width: 60%;">
    @if(auth()->check())
      <input type="text" name="code" placeholder="Enter Your Code (max 10)" class="form-control" maxlength="10">
    @else
      <input type="hidden" name="code">
    @endif
    <input type="submit" value="Short it!" class="btn" style="color: #222">
  </form>
  <div class="alert">
    @if($errors->has('url'))
      {{$errors->first('url')}}
    @endif
    @if(\Illuminate\Support\Facades\Session::has('global'))
      <p>All Done! Here's your URL:
        <a href="{{url()->current()}}/{{Session::get('global')}}" target="_blank">
          {{url()->current()}}/{{Session::get('global')}}
        </a>
      </p>
    @endif
  </div>
  <div class="last-url">
    <h2>Most Recent Shortened URL</h2>
    @include('table')
    @include('deleteModal')
  </div>
@endsection
@section('js')
  <script>
    $('#links')
      .dataTable({
        "searching": false,
        rowReorder : {
          selector: 'td:nth-child(1)'
        },
        responsive : true,
        "columns"  : [
          {"width": "1%"},
          {"width": "15%"},
          {"width": "30%"},
          {"width": "10%"},
          null,
          null
        ],
      });
    $('#links').on('click', '.delete', function (e) {
      e.preventDefault();
      let skey = $(this).attr('skey');
      $('#delete-modal').modal('show');
      $('#delete-modal').attr('skey', skey);
    });

    $('#delete-modal').on('click', '#delete', function () {
      let skey = $('#delete-modal').attr('skey');
      $.ajax({
        url   : '/delete/' + skey,
        method: 'GET'
      }).done(function (result) {
        $('#links').html(result);
        $('#delete-modal').modal('hide');
      });
    })
  </script>
@endsection