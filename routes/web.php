<?php
Route::get('/', 'MainController@index');
Route::post('make', 'LinkController@make');
Route::get('login', 'AuthController@showLoginForm')->name('login');
Route::post('login', 'AuthController@loginPost')->name('login.post');
Route::get('register', 'AuthController@showRegisterForm')->name('register');
Route::post('register', 'AuthController@registerPost')->name('register.post');
Route::get('logout', 'AuthController@logout')->name('logout');
Route::get('{code}', 'LinkController@open');
Route::get('/delete/{skey}', 'LinkController@delete');
